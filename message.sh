#!/bin/bash
TOKEN=$1
CHAT_ID=$2
MESSAGE=$3
URL="https://api.telegram.org/bot$TOKEN/sendMessage"
curl -s -X POST $URL -d chat_id=$CHAT_ID -d text="URL: $MESSAGE$4"