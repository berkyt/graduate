pipeline {
    agent {
        label 'aws-auto'
    }

    environment {
        DOCKER_REPO="020658203953.dkr.ecr.us-east-2.amazonaws.com"
        IMAGE_NAME="webpy-ecr"
        AWS_ACCESS_KEY = credentials('AWS_ACCESS_KEY')
        AWS_SECRET_KEY = credentials('AWS_SECRET_KEY')
        AWS_REGION = "us-east-2"
        TG_TOKEN = credentials('TG-TOKEN')
        TG_CHAT_ID = credentials('TG-CHAT_ID')
        CLUSTER = "eks_cluster_webpy"
        POD_SVC = "mypoddocker-webpy-svc"
    }


    stages {

        stage("Clone webpy repository") {
            steps {
                dir('webpy') {
                    git branch: 'main',
                        url: 'git@bitbucket.org:berkyt/webpy.git',
                        credentialsId: 'Jenkins-Bitbucket'
                }
                sh 'cp -r ./webpy/* ./docker/'
            }
        }


		stage('install Docker') {
			steps {
					sh '''
			            sudo yum -y update
                        sudo yum install docker -y
                        sudo service docker start
                        sudo service docker status
                    '''
            } 
        }

        stage ('AWS configure, Login to ECR') {
            steps {
                sh """
                    sudo yum install awscli -y
                    aws --version
                    aws configure set aws_access_key_id ${AWS_ACCESS_KEY}
                    aws configure set aws_secret_access_key ${AWS_SECRET_KEY}
                    aws configure set default.region ${AWS_REGION}
                    aws ecr get-login-password --region ${AWS_REGION} | sudo docker login --username AWS --password-stdin ${DOCKER_REPO}
                """
            }
        }

        stage ('Create Image and Docker push') {
            steps {
                sh """
                    cd docker
                    sudo docker build -t "${DOCKER_REPO}/${IMAGE_NAME}:${BUILD_NUMBER}" .
                    sudo docker build -t ${DOCKER_REPO}/${IMAGE_NAME}:latest .
                    sudo docker push ${DOCKER_REPO}/${IMAGE_NAME}:${BUILD_NUMBER}
                    sudo docker push ${DOCKER_REPO}/${IMAGE_NAME}:latest
                    cd ..
                """
            }
            
        }


        stage ('Install kubeconfig, connect to aws, deploy') {
            steps {
                sh """
                    bash message.sh "\${TG_TOKEN}" "\${TG_CHAT_ID}" "Confirm your choice in ${ env.JOB_NAME } ${ env.BUILD_NUMBER } (<${ env.BUILD_URL }|Open>)"
                """
                input "Do you want to update/create deployment?"
                sh """
                    curl -o kubectl https://amazon-eks.s3.us-west-2.amazonaws.com/1.19.6/2021-01-05/bin/linux/amd64/kubectl
                    sudo chmod +x ./kubectl
                    mkdir -p $HOME/bin && cp ./kubectl $HOME/bin/kubectl && export PATH=$PATH:$HOME/bin
                    kubectl version --short --client
                    aws eks update-kubeconfig --name ${CLUSTER} --region ${AWS_REGION}
                    kubectl get all
                    ls -l
                    pwd
                    sed -i "s|latest|${BUILD_NUMBER}|" deployment.yml
                    cat deployment.yml
                    kubectl apply -f deployment.yml
                    kubectl get all
                    kubectl get services ${POD_SVC} > services.txt
                """
            }
            
        }

        stage('Send message to Telegramm') {
            steps {
                    sh '''
                        cut -d" " -f10 services.txt
                        bash message.sh "\${TG_TOKEN}" "\${TG_CHAT_ID}" $(cut -d" " -f10 services.txt) ":8000"
                    '''
            }
        }


    }  
    
    post {
        failure {
            slackSend color: 'danger', message: "${ env.JOB_NAME } ${ env.BUILD_NUMBER } failed (<${ env.BUILD_URL }|Open>)"
        }

        success {
            slackSend color: 'good', message: "${ env.JOB_NAME} ${ env.BUILD_NUMBER } complete (<${ env.BUILD_URL }|Open>)"
        }

        aborted {
            slackSend color: 'danger', message: "${ env.JOB_NAME} ${ env.BUILD_NUMBER } aborted (<${ env.BUILD_URL }|Open>)"
        }
    }

}