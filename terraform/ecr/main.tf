terraform {
  backend "s3" {
    bucket         = "webpy-s3-bucket"
    key            = "ecr/terraform.tfstate"
    region         = "us-east-2"
    dynamodb_table = "webpy-terraform-ecr-lock"
  }
}

provider "aws" {
  region = "us-east-2"
}

