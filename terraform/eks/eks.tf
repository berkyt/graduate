resource "aws_eks_cluster" "aws_eks" {
  name     = "eks_cluster_webpy"
  role_arn = aws_iam_role.eks_cluster.arn
  version = 1.19


  vpc_config {
    security_group_ids = ["${aws_security_group.eks-cluster.id}"]  
    subnet_ids = ["subnet-0bcc8a10212484553", "subnet-0fef1a2e131442743"]
  }

  tags = {
    Name = "EKS_webpy"
  }
}
