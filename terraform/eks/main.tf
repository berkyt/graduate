# Creating IAM role for Kubernetes clusters to make calls to other AWS services on your behalf to manage the resources that you use with the service.
terraform {
  backend "s3" {
    bucket         = "webpy-s3-bucket"
    key            = "eks/terraform.tfstate"
    region         = "us-east-2"
    dynamodb_table = "webpy-terraform-eks-lock"
  }
}
