resource "aws_dynamodb_table" "dynamodb-terraform-state-lock-eks" {
  name           = "webpy-terraform-eks-lock"
  hash_key       = "LockID"
  read_capacity  = 8
  write_capacity = 8

  attribute {
    name = "LockID"
    type = "S"
  }
}

resource "aws_dynamodb_table" "dynamodb-terraform-state-lock-ecr" {
  name           = "webpy-terraform-ecr-lock"
  hash_key       = "LockID"
  read_capacity  = 8
  write_capacity = 8

  attribute {
    name = "LockID"
    type = "S"
  }
}
