provider "aws" {
  region = "us-east-2"
}

terraform {
  backend "s3" {
    bucket         = "webpy-s3-bucket"
    key            = "dynamodb/terraform.tfstate"
    region         = "us-east-2"
  }
}
