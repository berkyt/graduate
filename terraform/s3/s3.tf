resource "aws_s3_bucket" "webpy-bucket" {
  bucket = "webpy-s3-bucket"
  acl    = "private"

  tags = {
    Name        = "webpy-s3-bucket"
    
  }
}