provider "aws" {
  region = "us-east-2"
}

terraform {
  backend "s3" {
    bucket         = "terraform-bucket-ls34"
    key            = "s3/terraform.tfstate"
    region         = "us-east-2"
  }
}